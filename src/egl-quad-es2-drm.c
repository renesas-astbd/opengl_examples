
/*
static int init_drm(void)
{
	static const char *modules[] = {
			"nv", "i915", "radeon", "nouveau", "vmwgfx", "omapdrm", "exynos", "msm"
	};
	drmModeRes *resources;
	drmModeConnector *connector = NULL;
	drmModeEncoder *encoder = NULL;
	int i, area;

	for (i = 0; i < ARRAY_SIZE(modules); i++) {
		printf("trying to load module %s...", modules[i]);
		drm.fd = drmOpen(modules[i], NULL);
		if (drm.fd < 0) {
			printf("failed.\n");
		} else {
			printf("success.\n");
			break;
		}
	}

	if (drm.fd < 0) {
		printf("could not open drm device\n");
		return -1;
	}

	resources = drmModeGetResources(drm.fd);
	if (!resources) {
		printf("drmModeGetResources failed: %s\n", strerror(errno));
		return -1;
	}

	// find a connected connector:
	for (i = 0; i < resources->count_connectors; i++) {
		connector = drmModeGetConnector(drm.fd, resources->connectors[i]);
		if (connector->connection == DRM_MODE_CONNECTED) {
			// it's connected, let's use this!
			break;
		}
		drmModeFreeConnector(connector);
		connector = NULL;
	}

	if (!connector) {
		// we could be fancy and listen for hotplug events and wait for a connector..
		printf("no connected connector!\n");
		return -1;
	}

	// find highest resolution mode:
	for (i = 0, area = 0; i < connector->count_modes; i++) {
		drmModeModeInfo *current_mode = &connector->modes[i];
		int current_area = current_mode->hdisplay * current_mode->vdisplay;
		if (current_area > area) {
			drm.mode = current_mode;
			area = current_area;
		}
	}

	if (!drm.mode) {
		printf("could not find mode!\n");
		return -1;
	}

	//* find encoder:
	for (i = 0; i < resources->count_encoders; i++) {
		encoder = drmModeGetEncoder(drm.fd, resources->encoders[i]);
		if (encoder->encoder_id == connector->encoder_id)
			break;
		drmModeFreeEncoder(encoder);
		encoder = NULL;
	}

	if (!encoder) {
		printf("no encoder!\n");
		return -1;
	}

	drm.crtc_id = encoder->crtc_id;
	drm.connector_id = connector->connector_id;

	return 0;
}
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* egl (Embedded GL) supplants xgl/wgl intialization API */
/* See https://www.khronos.org/registry/egl/sdk/docs/man/html/eglIntro.xhtml */
#include <EGL/egl.h>        /* If missing install libegl1-mesa-dev */
#include <EGL/eglext.h>

#include <fcntl.h>

/* DRM structures */
char g_drm_szDeviceName[128] = "/dev/dri/card0";
int g_drm_file = 0;

/* EGL structures */
EGLDisplay g_egl_display;
EGLConfig  g_egl_config;
EGLSurface g_egl_surface;
EGLContext g_egl_context;


static void init_drm(void)
{
	g_drm_file = open(g_drm_szDeviceName, O_RDWR);
}

static void fini_drm(void)
{
	if(g_drm_file != 0)
		close(g_drm_file);
}

static void print_ext(EGLDisplay eglDisplay)
{
	const char* szExt = eglQueryString(eglDisplay, EGL_EXTENSIONS);
	printf("EGL Extensions:\n%s\n", szExt);
}

#define CASEERRORSTRING(tok) case tok: return #tok

const char* egl_error_to_string(int error)
{
    switch(error)
    {
        CASEERRORSTRING(EGL_SUCCESS);
        
        CASEERRORSTRING(EGL_FALSE);
        CASEERRORSTRING(EGL_TRUE);
        
        CASEERRORSTRING(EGL_NOT_INITIALIZED);
        
        CASEERRORSTRING(EGL_BAD_ACCESS);
        CASEERRORSTRING(EGL_BAD_ALLOC);
        CASEERRORSTRING(EGL_BAD_ATTRIBUTE);
        CASEERRORSTRING(EGL_BAD_CONFIG);
        CASEERRORSTRING(EGL_BAD_CONTEXT);
        CASEERRORSTRING(EGL_BAD_CURRENT_SURFACE);
        CASEERRORSTRING(EGL_BAD_DISPLAY);
        CASEERRORSTRING(EGL_BAD_PARAMETER);
        CASEERRORSTRING(EGL_BAD_MATCH);
        CASEERRORSTRING(EGL_BAD_SURFACE);

        CASEERRORSTRING(EGL_CONTEXT_LOST);

        default:
            return "<unknown>";
    }
}

static const char* egl_get_error_string()
{
	EGLint error = eglGetError();
	return egl_error_to_string(error);
}

static void print_config(EGLDisplay* eglDisplay, EGLConfig* config)
{
    
    int color_depth, red, green, blue, alpha;
    int depth, level;
    int swap_min, swap_max;
    int sample_buffers, samples_per_pixel;
    int stencil_bits;
    red = green = blue = alpha = 0;
    color_depth = 0;

    //Flush errors
    while(EGL_SUCCESS != eglGetError());

    printf("EGL Config %zu:\n", (size_t)config);

    if(!eglGetConfigAttrib(eglDisplay, config, EGL_RED_SIZE, &red))
    {
        int error = eglGetError();
        printf("\tFailed to get red bits 0x(%x) %s\r\n", error, egl_error_to_string(error));
    }
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_GREEN_SIZE, &green))
        printf("\tFailed to get green bits\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_BLUE_SIZE, &blue))
        printf("\tFailed to get blue bits\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_ALPHA_SIZE, &alpha))
        printf("\tFailed to get alpha bits\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_BUFFER_SIZE, &color_depth))
        printf("\tFailed to get color depth\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_DEPTH_SIZE, &depth))
        printf("\tFailed to get depth bits\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_LEVEL, &level))
        printf("\tFailed to get level\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_MAX_SWAP_INTERVAL, &swap_max))
        printf("\tFailed to get swap max\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_MIN_SWAP_INTERVAL, &swap_min))
        printf("\tFailed to get swap min\r\n");

    if(!eglGetConfigAttrib(eglDisplay, config, EGL_SAMPLE_BUFFERS, &sample_buffers))
        printf("\tFailed to get sample buffers\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_SAMPLES, &samples_per_pixel))
        printf("\tFailed to get samples per pixel\r\n");
    if(!eglGetConfigAttrib(eglDisplay, config, EGL_STENCIL_SIZE, &stencil_bits))
        printf("\tFailed to get samples per pixel\r\n");

    printf("\trgba     : %ib %i:%i:%i:%i\r\n", color_depth, red, green, blue, alpha);
    printf("\tdepth    : %i\r\n", depth);
    printf("\tstencil  : %i\r\n", stencil_bits);
    printf("\tsampling : %i, %i/pixel\r\n", sample_buffers, samples_per_pixel);
    printf("\tlevel    : %i\r\n", level);
    printf("\tswap int : %i .. %i\r\n", swap_min, swap_max);
}

static void init_egl_display(EGLConfig force_config)
{
    EGLint major, minor, configs;
    int i;
    EGLConfig egl_config[20];

    /* EGL Initialization */
    g_egl_display = eglGetDisplay((EGLNativeDisplayType)NULL);
    if(!eglInitialize(g_egl_display, &major, &minor))
    {
        printf("Failed to initialized EGL\n");
    }
    printf("Initialized EGL v%d.%d\n", major, minor);

    if(!eglBindAPI(EGL_OPENGL_ES_API))
    {
        printf("Failed to bind EGL to OpenGL ES API\n");
    }
    printf("Bound to OpenGL ES API\n");

    const EGLint config_attribs[] = {
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_RED_SIZE,   8,
        EGL_GREEN_SIZE, 8,
        EGL_BLUE_SIZE,  8,
        EGL_ALPHA_SIZE, 8,
        EGL_DEPTH_SIZE, 1,
        EGL_STENCIL_SIZE, 1,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_NONE
        };
    if(!eglChooseConfig(g_egl_display, config_attribs, &egl_config[0], sizeof(egl_config) / sizeof(egl_config[0]), &configs))
    {
        printf("Failed to choose EGL config\n");
    }
    printf("Found %i EGL configuration(s)\n", configs);
    for(i=0; i<configs; ++i)
    {
        print_config(g_egl_display, egl_config[i]);
    }
    if(force_config != (EGLConfig)-1)
        g_egl_config = force_config;
    else
        g_egl_config = egl_config[0];

    const EGLint context_attribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
        };

    g_egl_context = eglCreateContext(g_egl_display, g_egl_config, EGL_NO_CONTEXT, context_attribs);
    if(g_egl_context == 0)
    {
        printf("Failed to create EGL context\n");
    }
    printf("EGL context created (%p) using config %zu\n", g_egl_context, (size_t)g_egl_config);

    g_egl_surface = eglCreateWindowSurface(g_egl_display, g_egl_config, NULL, NULL);
    if(g_egl_surface == EGL_NO_SURFACE)
    {
        printf("Failed to create EGL window surface\n");
    }
    printf("EGL window surface created (%p)\n", g_egl_surface);


    if(!eglSurfaceAttrib(g_egl_display, NULL, EGL_SWAP_BEHAVIOR, EGL_BUFFER_DESTROYED))
    {
        int error = eglGetError();
        printf("Failed to set surface attribute EGL_BUFFER_DESTROYED, (0x%x) %s\n", error, egl_error_to_string(error));
    }

    if(!eglMakeCurrent(g_egl_display, NULL, NULL, g_egl_context))
    {
        int error = eglGetError();
        printf("Failed to make EGL context current, config (int)%i, error (0x%x) %s\n", g_egl_config, error, egl_error_to_string(error));
    }

    if(!eglSwapInterval(g_egl_display, 0))
    {
        int error = eglGetError();
        printf("Failed to set swap interval to 1, (0x%x) %s\n", error, egl_error_to_string(error));
    }

    printf("EGL context is ready to render\n");
}

int main(int argc, char* argv[])
{
	EGLint major, minor;

    printf("\n\nRenesas EGL/DRM reference\n");
    init_drm();
    //init_egl_display(-1);
    //print_ext(g_egl_display);
    fini_drm();
    return 0;
}
