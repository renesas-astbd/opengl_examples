/***
 * Renesas OpenGL ES2 on Weston reference application
 * Shannon Barber <shannon.barber@renesas.com>
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* Some Wayland API glue is still needed beyond EGL */
/* include wayland header before EGL headers as it affects some of EGL's declarations */
#include <wayland-client.h> /* If mising install libwayland-dev */
#include <wayland-egl.h>
#include <wayland-cursor.h>

/* egl (Embedded GL) supplants xgl/wgl intialization API */
/* See https://www.khronos.org/registry/egl/sdk/docs/man/html/eglIntro.xhtml */
#include <EGL/egl.h>        /* If missing install libegl1-mesa-dev */
//#include <EGL/eglext.h>

/* This samples uses OpenGL ES 2 */
/* See https://www.khronos.org/opengles/2_X/ for introduction to OpenGL ES2 */
#include <GLES2/gl2.h>       /* If missing install libgles2-mesa-dev */
//#include <GLES2/gl2ext.h>
//#include <GLES2/2platform.h>

#include <linux/input.h> /* for BTN_LEFT et. al. */
#include <linux/poll.h>  /* use poll() for pump_events */

#include <ncurses.h>     /* install libncurses5-dev if missing */
#include <time.h>


/* Application flags */
bool g_bFullscreen = false;
bool g_ShouldExit = false;
int g_UseConfig = -1;

/* Wayland/Weston structures */
struct wl_display       *g_wl_display;
struct wl_compositor    *g_wl_compositor;
struct wl_region        *g_wl_opaque_reqion;
struct wl_shell         *g_wl_shell;
struct wl_shell_surface *g_wl_shell_surface;
struct wl_surface       *g_wl_surface;
struct wl_window        *g_wl_window;

struct wl_seat          *g_wl_seat;
struct wl_pointer       *g_wl_pointer;
struct wl_shm           *g_wl_shm;
struct wl_cursor_theme  *g_wl_cursor_theme;
struct wl_cursor        *g_wl_default_cursor;
struct wl_surface       *g_wl_cursor_surface;

struct wl_callback *g_wl_callback;

/* Bridge between Wayland and EGL */
struct wl_egl_window *g_egl_window;

/* EGL structures */
EGLDisplay g_egl_display;
EGLConfig  g_egl_config;
EGLSurface g_egl_surface;
EGLContext g_egl_context;

/* OpenGL application data*/
GLuint g_width = 1280;
GLuint g_height = 720;
GLuint g_rotation_uniform;
GLuint g_vertices;
GLuint g_colors;
static gl_set_projection();
static gl_clear(void);
static gl_draw_quad(void);

/* Reference Application sub-functions */
static void init_display(void);
static void init_wl_display(void);
static void init_egl_display(void);
static void shutdown(void);
static void setup_gles(void);

/* Hooks for Wayland/Weston window management */
static void wl_registry_handler(void *data, struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version);
static void wl_registry_handler_remove(void *data, struct wl_registry *registry, uint32_t name);
static const struct wl_registry_listener g_registry_listener = {
    wl_registry_handler,
    wl_registry_handler_remove
};

static void handle_ping(void *data, struct wl_shell_surface *shell_surface, uint32_t serial);
static void handle_configure(void *data, struct wl_shell_surface *shell_surface, uint32_t edges, int32_t width, int32_t height);
static void handle_popup_done(void *data, struct wl_shell_surface *shell_surface);
static const struct wl_shell_surface_listener shell_surface_listener = {
    handle_ping,
    handle_configure,
    handle_popup_done
};

static void surface_enter(void *data, struct wl_surface *wl_surface, struct wl_output *wl_output);
static void surface_leave(void *data, struct wl_surface *wl_surface, struct wl_output *output);
static const struct wl_surface_listener surface_listener = {
    surface_enter,
    surface_leave
};

static void seat_handle_capabilities(void *data, struct wl_seat *seat, enum wl_seat_capability caps);
static const struct wl_seat_listener seat_listener = {
    seat_handle_capabilities,
};

static void pointer_handle_enter(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface, wl_fixed_t sx, wl_fixed_t sy);
static void pointer_handle_leave(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface);
static void pointer_handle_motion(void *data, struct wl_pointer *pointer, uint32_t time, wl_fixed_t sx, wl_fixed_t sy);
static void pointer_handle_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial, uint32_t time, uint32_t button, uint32_t state);
static void pointer_handle_axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value);
static const struct wl_pointer_listener pointer_listener = {
    pointer_handle_enter,
    pointer_handle_leave,
    pointer_handle_motion,
    pointer_handle_button,
    pointer_handle_axis,
};

const char* egl_error_to_string(int error);
static void redraw(void *data, struct wl_callback *callback, uint32_t time);
static const struct wl_callback_listener frame_listener = {
    redraw
};

static gl_clear(void)
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClearDepthf(1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

static gl_set_projection(void)
{
    glViewport(0, 0, g_width, g_height);
}

static gl_draw_quad(void)
{
    static const GLfloat verts[4][3] = {
        { -0.75f, -0.75f, 0.0f },
        {  0.75f, -0.75f, 0.0f },
        {  0.75f,  0.75f, 0.0f },
        { -0.75f,  0.75f, 0.0f }
    };
    static const GLubyte indices[] = {
        0,1,2,
        0,2,3
    };
    static const GLfloat colors[4][3] = {
        { 1, 0, 0 },
        { 0, 1, 0 },
        { 0, 0, 1 },
        { 1, 1, 0 }
    };
    static GLfloat angle;
    GLfloat rotation[4][4] = {
        { 1, 0, 0, 0 },
        { 0, 1, 0, 0 },
        { 0, 0, 1, 0 },
        { 0, 0, 0, 1 }
    };

    angle += 1.0f/6.283185307f;
    rotation[0][0] =  cos(angle);
    rotation[0][2] =  sin(angle);
    rotation[2][0] = -sin(angle);
    rotation[2][2] =  cos(angle);
    glUniformMatrix4fv(g_rotation_uniform, 1, GL_FALSE, (GLfloat *) rotation);

    glVertexAttribPointer(g_vertices, 3, GL_FLOAT, GL_FALSE, 0, verts);
    glVertexAttribPointer(g_colors, 3, GL_FLOAT, GL_FALSE, 0, colors);
    glEnableVertexAttribArray(g_vertices);
    glEnableVertexAttribArray(g_colors);
    glDrawElements(GL_TRIANGLES, sizeof(indices)/sizeof(indices[1]), GL_UNSIGNED_BYTE, indices);
    glDisableVertexAttribArray(g_vertices);
    glDisableVertexAttribArray(g_colors);
}

static void render()
{
    gl_clear();
    gl_set_projection();
    gl_draw_quad();
    if(!eglSwapBuffers(g_egl_display, g_egl_surface))
    {
        int error = eglGetError();
        printf("Swap buffers error (0x%x) %s\r\n", error, egl_error_to_string(error)); refresh();
    }
    int ret = wl_display_roundtrip(g_wl_display);
}

static void redraw(void *data, struct wl_callback *callback, uint32_t time)
{
    /*
    //assert(g_wl_callback == callback);
    g_wl_callback = NULL;

    if (callback) wl_callback_destroy(callback);

    g_wl_callback = wl_surface_frame(g_wl_surface);
    wl_callback_add_listener(g_wl_callback, &frame_listener, NULL);

    render();
    */
}

static void configure_callback(void *data, struct wl_callback *callback, uint32_t  time)
{
    wl_callback_destroy(callback);

    if (g_wl_callback == NULL)
    {
        redraw(data, NULL, time);
    }
}

static struct wl_callback_listener configure_callback_listener = {
    configure_callback,
};

static pump_events()
{
    struct pollfd pfd[1];

    pfd[0].fd = wl_display_get_fd(g_wl_display);
    pfd[0].events = POLLIN;
    poll(pfd, 1, 0);

    if (pfd[0].revents & POLLIN)
    {
        return wl_display_dispatch(g_wl_display);
    }
    else
    {
        return wl_display_dispatch_pending(g_wl_display);
    }
}

static process_args(int argc, char* argv[])
{
    int i;
    for(i=1; i<argc; ++i)
    {
        char* opt = argv[i];
        if(0 == strcmp(opt, "--config"))
        {
            if(i+1 < argc)
            {
                int config = -1;
                sscanf(argv[i+1], "%i", &config);
                g_UseConfig = config;
            }
        }
    }
}

int main(int argc, char* argv[])
{
    int row, col;
    unsigned int frames = 0;
    time_t current_time = {0};

    g_ShouldExit = false;

    process_args(argc, argv);

    /*ncurses init*/
    initscr();
    atexit(&shutdown);
    getmaxyx(stdscr,row,col);
    move(0,0);

    printf("Renesas OpenGL ES2 on Weston Reference Application\r\n"); refresh();

    init_display();
    setup_gles();

    /* OpenGL is now initialized */
    printf("Entering rendering loop ...\r\n"); refresh();
    while(!g_ShouldExit)
    {
        //g_ShouldExit = (-1 == wl_display_dispatch(g_wl_display));
        move(row-1,0);
        printw("Frames: %d", frames++); refresh();
        //g_ShouldExit = (-1 == pump_events());
        //usleep(10000);
        render();
    }

    printf("Shutting down ...\r\n"); refresh();
    //Called by atexit shutdown();
    return 0;
}

static void setup_gles(void)
{
    GLuint frag, vert;
    GLuint program;
    GLint status;
    GLchar errlog[1024];
    GLint len = 0;


    const GLchar *vert_shader_text =
        "uniform mat4 rotation;\n"
        "attribute vec4 pos;\n"
        "attribute vec4 color;\n"
        "varying vec4 v_color;\n"
        "void main() {\n"
        "  gl_Position = rotation * pos;\n"
        "  v_color = color;\n"
        "}\n";

    const GLchar *frag_shader_text =
        "precision mediump float;\n"
        "varying vec4 v_color;\n"
        "void main() {\n"
        "  gl_FragColor = v_color;\n"
        "}\n";

    vert = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vert, 1, &vert_shader_text, NULL);
    glCompileShader(vert);

    frag = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frag, 1, &frag_shader_text, NULL);
    glCompileShader(frag);

    glGetShaderiv(vert, GL_COMPILE_STATUS, &status);
    if(status != GL_TRUE)
    {
        glGetProgramInfoLog(program, sizeof(errlog), &len, errlog);
        printf("Failed to compile vertex shader (%d)\r\n", status); refresh();
        exit(-1);
    }
    glGetShaderiv(frag, GL_COMPILE_STATUS, &status);
    if(status != GL_TRUE)
    {
        glGetProgramInfoLog(program, sizeof(errlog), &len, errlog);
        printf("Failed to compile fragment shader (%d)\r\n", status); refresh();
        exit(-1);
    }

    program = glCreateProgram();
    glAttachShader(program, vert);
    glAttachShader(program, frag);
    glLinkProgram(program);
    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if(status != GL_TRUE)
    {
        glGetProgramInfoLog(program, sizeof(errlog), &len, errlog);
        printf("Failed to link program (%d)\r\n\t%s\r\n", status, errlog); refresh();
        exit(-1);
    }

    glUseProgram(program);

    g_vertices = 0;
    g_colors = 1;

    g_rotation_uniform = glGetUniformLocation(program, "rotation");
    g_vertices = glGetAttribLocation(program, "pos");
    g_colors = glGetAttribLocation(program, "color");

    glViewport(0, 0, g_width, g_height);
}

static void init_display(void)
{
    init_wl_display();
    init_egl_display();
}

static void init_wl_display(void)
{
    struct wl_callback *callback;

    g_wl_display = wl_display_connect(NULL);
    if(g_wl_display == 0)
    {
        printf("Failed to connect to Wayland display\r\n"); refresh();
        exit(-1);
    }
    printf("Connected to Wayland display (%p)\r\n", g_wl_display); refresh();

    struct wl_registry *registry;
    registry = wl_display_get_registry(g_wl_display);
    wl_registry_add_listener(registry, &g_registry_listener, &g_wl_display);
    wl_display_roundtrip(g_wl_display); /* Invokes the register handlers so we get the compositor */
    wl_registry_destroy(registry);

    g_wl_surface = wl_compositor_create_surface(g_wl_compositor);
    if(g_wl_surface == 0)
    {
        printf("Failed to create weston surface\r\n"); refresh();
        exit(-1);
    }
    printf("Wayland surface created (%p)\r\n", g_wl_surface); refresh();
    wl_surface_add_listener(g_wl_surface, &surface_listener, NULL /*cookie*/);

    g_egl_window = wl_egl_window_create(g_wl_surface, g_width, g_height);
    if(g_egl_window == 0)
    {
        printf("Failed to create Wayland EGL window\r\n"); refresh();
        exit(-1);
    }
    printf("Wayland EGL window created (%p)\r\n", g_egl_window); refresh();

    g_wl_opaque_reqion = wl_compositor_create_region(g_wl_compositor);
    wl_region_add(g_wl_opaque_reqion, 0, 0, g_width, g_height);
    wl_surface_set_opaque_region(g_wl_surface, g_wl_opaque_reqion);

    g_wl_shell_surface = wl_shell_get_shell_surface(g_wl_shell, g_wl_surface);
    wl_shell_surface_add_listener(g_wl_shell_surface, &shell_surface_listener, g_wl_window);
    
    wl_shell_surface_set_title(g_wl_shell_surface, "Renesas OpenGL ES 2 Sample");

    g_wl_cursor_surface = wl_compositor_create_surface(g_wl_compositor);

    if(g_bFullscreen)
    {
        //Full-screen
        wl_shell_surface_set_fullscreen(g_wl_shell_surface, WL_SHELL_SURFACE_FULLSCREEN_METHOD_DEFAULT, 0, NULL);
    }
    else
    {
        //Windowed
        wl_shell_surface_set_toplevel(g_wl_shell_surface);
        handle_configure(g_wl_window, g_wl_shell_surface, 0, g_width, g_height);
    }

    /*
    callback = wl_display_sync(g_wl_display);
    wl_callback_add_listener(callback, &configure_callback_listener, NULL);
    */
}

#define CASEERRORSTRING(tok) case tok: return #tok

const char* egl_error_to_string(int error)
{
    switch(error)
    {
        CASEERRORSTRING(EGL_SUCCESS);
        CASEERRORSTRING(EGL_FALSE);
        CASEERRORSTRING(EGL_BAD_CONFIG);
        CASEERRORSTRING(EGL_BAD_DISPLAY);
        CASEERRORSTRING(EGL_BAD_ATTRIBUTE);
        CASEERRORSTRING(EGL_NOT_INITIALIZED);
        CASEERRORSTRING(EGL_BAD_PARAMETER);
        CASEERRORSTRING(EGL_BAD_MATCH);

        default:
            return "<unknown>";
    }
}

static void print_config(EGLDisplay* display, EGLConfig* config)
{
    
    int color_depth, red, green, blue, alpha;
    int depth, level;
    int swap_min, swap_max;
    red = green = blue = alpha = 0;
    color_depth = 0;

    //Flush errors
    while(EGL_SUCCESS != eglGetError());

    printf("EGL Config %i:\r\n", (int)config); refresh();

    if(!eglGetConfigAttrib(display, config, EGL_RED_SIZE, &red))
    {
        int error = eglGetError();
        printf("\tFailed to get red bits 0x(%x) %s\r\n", error, egl_error_to_string(error)); refresh();
    }
    if(!eglGetConfigAttrib(display, config, EGL_GREEN_SIZE, &green))
        printf("\tFailed to get green bits\r\n"); refresh();
    if(!eglGetConfigAttrib(display, config, EGL_BLUE_SIZE, &blue))
        printf("\tFailed to get blue bits\r\n"); refresh();
    if(!eglGetConfigAttrib(display, config, EGL_ALPHA_SIZE, &alpha))
        printf("\tFailed to get alpha bits\r\n"); refresh();
    if(!eglGetConfigAttrib(display, config, EGL_BUFFER_SIZE, &color_depth))
        printf("\tFailed to get color depth\r\n"); refresh();
    if(!eglGetConfigAttrib(display, config, EGL_DEPTH_SIZE, &depth))
        printf("\tFailed to get depth bits\r\n"); refresh();
    if(!eglGetConfigAttrib(display, config, EGL_LEVEL, &level))
        printf("\tFailed to get level\r\n"); refresh();
    if(!eglGetConfigAttrib(display, config, EGL_MAX_SWAP_INTERVAL, &swap_max))
        printf("\tFailed to get swap max\r\n"); refresh();
    if(!eglGetConfigAttrib(display, config, EGL_MIN_SWAP_INTERVAL, &swap_min))
        printf("\tFailed to get swap min\r\n"); refresh();

    printf("\trgba    : %ib %i:%i:%i:%i\r\n", color_depth, red, green, blue, alpha); refresh();
    printf("\tdepth   : %i\r\n", depth); refresh();
    printf("\tlevel   : %i\r\n", level); refresh();
    printf("\tswap int: %i .. %i\r\n", swap_min, swap_max); refresh();
}



static void init_egl_display(void)
{
    EGLint major, minor, configs;
    int i;
    EGLConfig egl_config[20];

    /* EGL Initialization */
    g_egl_display = eglGetDisplay((EGLNativeDisplayType)g_wl_display);
    if(!eglInitialize(g_egl_display, &major, &minor))
    {
        printf("Failed to initialized EGL\r\n"); refresh();
        exit(-1);
    }
    printf("Initialized EGL v%d.%d\r\n", major, minor); refresh();

    if(!eglBindAPI(EGL_OPENGL_ES_API))
    {
        printf("Failed to bind EGL to OpenGL ES API\r\n"); refresh();
        exit(-1);
    }
    printf("Bound to OpenGL ES API\r\n"); refresh();

    const EGLint config_attribs[] = {
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_RED_SIZE,   6,
        EGL_GREEN_SIZE, 6,
        EGL_BLUE_SIZE,  6,
        EGL_ALPHA_SIZE, 0,
        EGL_DEPTH_SIZE, 1,
        EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
        EGL_NONE
        };
    if(!eglChooseConfig(g_egl_display, config_attribs, &egl_config[0], sizeof(egl_config) / sizeof(egl_config[0]), &configs))
    {
        printf("Failed to choose EGL config\r\n"); refresh();
        exit(-1);
    }
    printf("Found %i EGL configuration(s)\r\n", configs); refresh();
    for(i=0; i<configs; ++i)
    {
        print_config(g_egl_display, egl_config[i]);
    }
    if(g_UseConfig != -1)
        g_egl_config = (EGLConfig)g_UseConfig;
    else
        g_egl_config = egl_config[0];

    const EGLint context_attribs[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
        };

    g_egl_context = eglCreateContext(g_egl_display, g_egl_config, EGL_NO_CONTEXT, context_attribs);
    if(g_egl_context == 0)
    {
        printf("Failed to create EGL context\r\n"); refresh();
        exit(-1);
    }
    printf("EGL context created (%p) using config %i\r\n", g_egl_context, (int)g_egl_config); refresh();

    g_egl_surface = eglCreateWindowSurface(g_egl_display, g_egl_config, g_egl_window, NULL);
    if(g_egl_surface == EGL_NO_SURFACE)
    {
        printf("Failed to create EGL window surface\r\n"); refresh();
        exit(-1);
    }
    printf("EGL window surface created (%p)\r\n", g_egl_surface); refresh();

    if(!eglMakeCurrent(g_egl_display, g_egl_surface, g_egl_surface, g_egl_context))
    {
        int error = eglGetError();
        printf("Failed to make EGL context current, config (int)%i, error (0x%x) %s\r\n", g_egl_config, error, egl_error_to_string(error)); refresh();
    }

    if(!eglSwapInterval(g_egl_display, 0))
    {
        int error = eglGetError();
        printf("Failed to set swap interval to 1, (0x%x) %s\n", error, egl_error_to_string(error));
    }


    if(!eglSurfaceAttrib(g_egl_display, g_egl_surface, EGL_SWAP_BEHAVIOR, EGL_BUFFER_DESTROYED))
    {
        int error = eglGetError();
        printf("Failed to set surface attribute EGL_BUFFER_DESTROYED, (0x%x) %s\n", error, egl_error_to_string(error));
    }

    printf("EGL context is ready to render\r\n"); refresh();
}

static void shutdown(void)
{
    endwin(); //clean-up ncurses
    if(g_egl_display != 0)
    {
        eglMakeCurrent(g_egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglTerminate(g_egl_display);
        eglReleaseThread();
    }
    if(g_wl_display != 0)
    {
        wl_display_disconnect(g_wl_display);
    }
}

static void wl_registry_handler(void *data, struct wl_registry *registry, uint32_t name, const char *interface, uint32_t version)
{
    struct display *d = data;
    printf("\tWayland window registered %s\r\n", interface); refresh();
    if (strcmp(interface, "wl_compositor") == 0)
    {
        g_wl_compositor = wl_registry_bind(registry, name, &wl_compositor_interface, 1);
    }
    else if (strcmp(interface, "wl_shell") == 0)
    {
        g_wl_shell = wl_registry_bind(registry, name, &wl_shell_interface, 1);
    }
    else if (strcmp(interface, "wl_seat") == 0)
    {
        g_wl_seat = wl_registry_bind(registry, name, &wl_seat_interface, 1);
        wl_seat_add_listener(g_wl_seat, &seat_listener, d);
    }
    else if (strcmp(interface, "wl_shm") == 0)
    {
        g_wl_shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
        g_wl_cursor_theme = wl_cursor_theme_load(NULL, 32, g_wl_shm);
        g_wl_default_cursor = wl_cursor_theme_get_cursor(g_wl_cursor_theme, "left_ptr");
    }
}

static void wl_registry_handler_remove(void *data, struct wl_registry *registry, uint32_t name)
{

}

static void handle_ping(void *data, struct wl_shell_surface *shell_surface, uint32_t serial)
{
    wl_shell_surface_pong(shell_surface, serial);
}

static void handle_configure(void *data, struct wl_shell_surface *shell_surface, uint32_t edges, int32_t width, int32_t height)
{
    printf("Wayland shell configure\r\n"); refresh();
    g_width = width;
    g_height = height;
    wl_egl_window_resize(g_egl_window, width, height, 0, 0);
}

static void handle_popup_done(void *data, struct wl_shell_surface *shell_surface)
{
    printf("Wayland shell pop-up\r\n"); refresh();
}

static void surface_enter(void *data, struct wl_surface *wl_surface, struct wl_output *wl_output)
{

}

static void surface_leave(void *data, struct wl_surface *wl_surface, struct wl_output *output)
{

}

static void seat_handle_capabilities(void *data, struct wl_seat *seat, enum wl_seat_capability caps)
{
    if ((caps & WL_SEAT_CAPABILITY_POINTER) && !g_wl_pointer)
    {
        g_wl_pointer = wl_seat_get_pointer(seat);
        wl_pointer_add_listener(g_wl_pointer, &pointer_listener, NULL);
    }
    else if (!(caps & WL_SEAT_CAPABILITY_POINTER) && g_wl_pointer)
    {
        wl_pointer_destroy(g_wl_pointer);
        g_wl_pointer = NULL;
    }

    /*if ((caps & WL_SEAT_CAPABILITY_KEYBOARD) && !d->keyboard)
    {
        d->keyboard = wl_seat_get_keyboard(seat);
        wl_keyboard_add_listener(d->keyboard, &keyboard_listener, d);
    }
    else if (!(caps & WL_SEAT_CAPABILITY_KEYBOARD) && d->keyboard)
    {
        wl_keyboard_destroy(d->keyboard);
        d->keyboard = NULL;
    }*/
}

static void pointer_handle_enter(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface, wl_fixed_t sx, wl_fixed_t sy)
{
    struct wl_buffer *buffer;
    struct wl_cursor_image *image;

    if (g_bFullscreen)
    {
        wl_pointer_set_cursor(pointer, serial, NULL, 0, 0);
    }
    else if (g_wl_default_cursor)
    {
        image = g_wl_default_cursor->images[0];
        buffer = wl_cursor_image_get_buffer(image);
        wl_pointer_set_cursor(pointer, serial, g_wl_cursor_surface, image->hotspot_x, image->hotspot_y);
        wl_surface_attach(g_wl_cursor_surface, buffer, 0, 0);
        wl_surface_damage(g_wl_cursor_surface, 0, 0, image->width, image->height);
        wl_surface_commit(g_wl_cursor_surface);
    }
}

static void pointer_handle_leave(void *data, struct wl_pointer *pointer, uint32_t serial, struct wl_surface *surface)
{
}

static void pointer_handle_motion(void *data, struct wl_pointer *pointer, uint32_t time, wl_fixed_t sx, wl_fixed_t sy)
{
}

static void pointer_handle_button(void *data, struct wl_pointer *wl_pointer, uint32_t serial, uint32_t time, uint32_t button, uint32_t state)
{
    if (button == BTN_LEFT && state == WL_POINTER_BUTTON_STATE_PRESSED)
    {
        wl_shell_surface_move(g_wl_shell_surface, g_wl_seat, serial);
    }
}

static void pointer_handle_axis(void *data, struct wl_pointer *wl_pointer, uint32_t time, uint32_t axis, wl_fixed_t value)
{
}
