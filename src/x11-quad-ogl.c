/***
 *
 * Renesas OpenGL X11 Reference Application
 *
 *    Shannon Barber <shannon.barber@renesas.com>
 *
 */

/*
sudo apt-get install libx11-dev ........... for X11/Xlib.h
sudo apt-get install mesa-common-dev....... for GL/glx.h
sudo apt-get install libglu1-mesa-dev ..... for GL/glu.h
sudo apt-get install libxrandr-dev ........ for X11/extensions/Xrandr.h
sudo apt-get install libxi-dev ............ for X11/extensions/XInput.h
*/

#include <X11/Xlib.h> /* For XOpenDisplay() */
#include <GL/glx.h>   /* For glXChooseVisual(), glXCreateContext(), et. al. */
#include <GL/gl.h>    /* OpenGL API */
#include <GL/glu.h>   /* glU API, gluLookAt et. al. */
#include <stdio.h>    /* For printf() */
#include <stdlib.h>   /* For exit() */
#include <signal.h>   /* For signal, to handle ctrl-C */

static const char title[] = "Renesas R-Car R8A779x OpenGL ES2 Reference Application";

static int               g_shouldexit = 1;
static Display*          g_display;
static Window            g_window;
static GLXContext        g_glc;

/* OpenGL to X11 binding */
static glx_initialize(void);
static glx_shutdown(void);

/* OpenGL sub-funtions */
static gl_set_projection(XWindowAttributes* gwa);
static gl_clear(void);
static gl_draw_quad(void);

static void sig_handler(int);

int main()
{
   XEvent xev;
   XWindowAttributes gwa;
   g_shouldexit = 0;

   struct sigaction act;
   act.sa_handler = sig_handler;
   sigaction(SIGINT, &act, NULL);

   printf("GLX Initialization\n");
   glx_initialize();

   printf("Render loop ...\n");
   while(!g_shouldexit)
   {
      long mask = ExposureMask|KeyPressMask|KeyReleaseMask|ButtonPressMask|ButtonReleaseMask;
      if(XCheckWindowEvent(g_display, g_window, mask, &xev))
      {
          switch(xev.type)
          {
              case Expose:
                 break;
              case KeyPress:
                 g_shouldexit = 1;
                 break;
              case ButtonPress:
                 {
                    XButtonEvent* btn = (XButtonEvent*)&xev;
                    printf("Button press %d,%d\n", btn->x, btn->y);
                 } break;
              default:
                 printf("Event: 0x%x (%d)\n", xev.type, xev.type);
                 break;
          }
      }

      XGetWindowAttributes(g_display, g_window, &gwa);
      gl_set_projection(&gwa);
      gl_clear();
      gl_draw_quad();
      glXSwapBuffers(g_display, g_window);
      usleep(10000);
   }

   printf("Shutdown ...\n");
   glx_shutdown();
}

static glx_initialize(void)
{
   Window        rootwindow = {0};
   GLint         att[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
   XVisualInfo*  visualinfo = 0;
   Colormap                cmap = {0};
   XSetWindowAttributes    swa = {0};

   g_display = XOpenDisplay(NULL);
   if(g_display == NULL)
   {
      printf("Unable to open X display\n");
      exit(0);
   }

   rootwindow = DefaultRootWindow(g_display);
   //No error checking?

   visualinfo = glXChooseVisual(g_display, 0, att);
   if(visualinfo == NULL)
   {
      printf("Unable to choose visual display\n");
      exit(0);
   }
   else
   {
      printf("Visual 0x%x selected\n", (unsigned int)visualinfo->visualid);
   }

   cmap = XCreateColormap(g_display, rootwindow, visualinfo->visual, AllocNone);
   swa.colormap = cmap;
   swa.event_mask = ExposureMask | KeyPressMask;

   g_window = XCreateWindow(g_display, rootwindow, 0, 0, 600, 600, 0, visualinfo->depth, InputOutput, visualinfo->visual, CWColormap | CWEventMask, &swa);
   XMapWindow(g_display, g_window);
   XStoreName(g_display, g_window, title);
   g_glc = glXCreateContext(g_display, visualinfo, NULL, GL_TRUE);
   glXMakeCurrent(g_display, g_window, g_glc);
}

static glx_shutdown(void)
{
   glXMakeCurrent(g_display, None, NULL);
   glXDestroyContext(g_display, g_glc);
   XDestroyWindow(g_display, g_window);
   XCloseDisplay(g_display);
}

static gl_clear(void)
{
   glEnable(GL_DEPTH_TEST);
   glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

static gl_set_projection(XWindowAttributes* gwa)
{
   //GLfloat ratio = gwa->width / gwa->height;
   GLfloat hratio = gwa->height / 600.0f;
   GLfloat wratio = gwa->width / 600.0f;
   glViewport(0, 0, gwa->width, gwa->height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(-wratio, wratio, -hratio, hratio, 1.0f, 20.0f);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluLookAt(1.0f, 1.0f, 10.0f,  0.0f, 0.0f, 0.0f,  0.0f, 1.0f, 0.0f);
}


GLfloat angle = 0.0f;
static gl_draw_quad(void)
{
   glMatrixMode(GL_MODELVIEW);
   glRotatef(angle, 0.0f, 1.0f, 0.0f);
   angle += 1.0f;

   glBegin(GL_QUADS);
    glColor3f(1.0f, 0.0f, 0.0f); glVertex3f(-0.75f, -0.75f, 0.0f);
    glColor3f(0.0f, 1.0f, 0.0f); glVertex3f( 0.75f, -0.75f, 0.0f);
    glColor3f(0.0f, 0.0f, 1.0f); glVertex3f( 0.75f,  0.75f, 0.0f);
    glColor3f(1.0f, 1.0f, 0.0f); glVertex3f(-0.75f,  0.75f, 0.0f);
   glEnd();
}

static void sig_handler(int sig)
{
   g_shouldexit = 1;
   //printf("sig: %d\n", sig);
}
