
SRC_DIRS=src
INC_DIRS=$(SRC_DIRS)

SRCS = $(wildcard src/*.c)

PROGRAMS=$(subst src/,out/,$(SRCS:.c=))

all: $(PROGRAMS)

info:
	@printf "Programs: \n"
	@printf "\t%s\n" $(PROGRAMS)

out/egl-%: src/egl-%.c
	@mkdir -p out
	$(CC) -o $@ $< -lwayland-client -lwayland-cursor -lwayland-egl -lEGL -lGLESv2 -lm -lncurses

out/x11-%: src/x11-%.c
	@mkdir -p out
	$(CC) -o $@ $< -lX11 -lGL -lGLU -lncurses

out/lsglext: src/lsglext.c
	@mkdir -p out
	$(CC) -o $@ $< -ldl -lEGL

clean:
	rm -rf out *.o

vpath: %.c ./src


out/egl-quad-es2: src/egl-quad-es2.c
